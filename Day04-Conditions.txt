Control flow
 * conditionals
 * Looping
 
Conditions
 - True
 - False
 
True,False - booleans

In [29]: 5 > 2
Out[29]: True

In [30]: 2 > 5
Out[30]: False

In [31]: type(True)
Out[31]: bool

In [32]: type(False)
Out[32]: bool

In [33]: 

## while writing block we dont have {}

# other programming languages
if <condition>
{

}
else
{

}

## python
indentation - spaces and tabs

reference:
https://www.python.org/dev/peps/pep-0008/
https://www.youtube.com/watch?v=wf-BqAjZb8M&t=33s




