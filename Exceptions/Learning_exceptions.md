# Exceptions/Error Handling
# preproduction - developer -> QA  
# python - try..except..else..finally# 2.x
# various exceptions supported in pythons

In [1]: import exceptions as e

In [2]: e.
     e.ArithmeticError           e.ImportError               e.PendingDeprecationWarning e.UnicodeDecodeError
     e.AssertionError            e.ImportWarning             e.ReferenceError            e.UnicodeEncodeError
     e.AttributeError            e.IndentationError          e.RuntimeError              e.UnicodeError
     e.BaseException             e.IndexError                e.RuntimeWarning            e.UnicodeTranslateError
     e.BufferError               e.IOError                   e.StandardError             e.UnicodeWarning
     e.BytesWarning              e.KeyboardInterrupt         e.StopIteration             e.UserWarning
     e.DeprecationWarning        e.KeyError                  e.SyntaxError               e.ValueError
     e.EnvironmentError          e.LookupError               e.SyntaxWarning             e.Warning
     e.EOFError                  e.MemoryError               e.SystemError               e.WindowsError
     e.Exception                 e.NameError                 e.SystemExit                e.ZeroDivisionError
     e.FloatingPointError        e.NotImplementedError       e.TabError
     e.FutureWarning             e.OSError                   e.TypeError
     e.GeneratorExit             e.OverflowError             e.UnboundLocalError
# 3.x

In [2]: import builtins as e

In [3]: e.
            abs()                     BrokenPipeError           ConnectionError           enumerate
            all                       BufferError               ConnectionRefusedError    EnvironmentError
            any                       bytearray                 ConnectionResetError      EOFError
            ArithmeticError           bytes                     copyright                 eval
            ascii                     BytesWarning              credits                   Exception
            AssertionError            callable                  delattr                   exec
            AttributeError            ChildProcessError         DeprecationWarning        exit                      >
            BaseException             chr                       dict                      False
            bin                       classmethod               dir                       FileExistsError
            BlockingIOError           compile                   display                   FileNotFoundError
            bool                      complex                   divmod                    filter
            breakpoint                ConnectionAbortedError    Ellipsis                  float

```python
# case I

num1 = int(input("please enter the num1:"))
num2 = int(input("please enter the num2:"))
result = num1/num2
print ("result is - {}".format(result))
```

    please enter the num1:10
    please enter the num2:2
    result is - 5.0
    


```python
# case II

num1 = int(input("please enter the num1:"))
num2 = int(input("please enter the num2:"))
result = num1/num2
print ("result is - {}".format(result))
```

    please enter the num1:ten
    


    ---------------------------------------------------------------------------

    ValueError                                Traceback (most recent call last)

    <ipython-input-2-5981b2c885de> in <module>
          1 # case II
          2 
    ----> 3 num1 = int(input("please enter the num1:"))
          4 num2 = int(input("please enter the num2:"))
          5 result = num1/num2
    

    ValueError: invalid literal for int() with base 10: 'ten'



```python
# Case III

num1 = int(input("please enter the num1:"))
num2 = int(input("please enter the num2:"))
result = num1/num2
print ("result is - {}".format(result))
```

    please enter the num1:10
    please enter the num2:0
    


    ---------------------------------------------------------------------------

    ZeroDivisionError                         Traceback (most recent call last)

    <ipython-input-3-966ebc4dedff> in <module>
          3 num1 = int(input("please enter the num1:"))
          4 num2 = int(input("please enter the num2:"))
    ----> 5 result = num1/num2
          6 print ("result is - {}".format(result))
    

    ZeroDivisionError: division by zero



```python
## handling exceptions
# try -> code which can create errors.
# else -> if try block is true(no errors),then what. 
# except -> what to do with exception.
```


```python
# type 1
try:
    num1 = int(input("please enter the num1:"))
    num2 = int(input("please enter the num2:"))
    result = num1/num2
except:
    print ("Please enter a number,make sure denominator is non-zero")
else:
    print ("result is - {}".format(result))
```

    please enter the num1:10
    please enter the num2:2
    result is - 5.0
    


```python
# type 2
try:
    num1 = int(input("please enter the num1:"))
    num2 = int(input("please enter the num2:"))
    result = num1/num2
except:
    print ("Please enter a number,make sure denominator is non-zero")
else:
    print ("result is - {}".format(result))
```

    please enter the num1:ten
    Please enter a number,make sure denominator is non-zero
    


```python
# type 3
try:
    num1 = int(input("please enter the num1:"))
    num2 = int(input("please enter the num2:"))
    result = num1/num2
except:
    print ("Please enter a number,make sure denominator is non-zero")
else:
    print ("result is - {}".format(result))
```

    please enter the num1:10
    please enter the num2:0
    Please enter a number,make sure denominator is non-zero
    
## except block is handling all exceptions

```python
## i want to capture only the ValueError and ZeroDivision exceptions

try:
    num1 = int(input("please enter the num1:"))
    num2 = int(input("please enter the num2:"))
    result = num1/num2
except (ValueError,ZeroDivisionError):  # we want to only capture - ValueError and ZeroDivisionError.
    print ("Please enter a number,make sure denominator is non-zero")
else:
    print ("result is - {}".format(result))
```

    please enter the num1:ten
    Please enter a number,make sure denominator is non-zero
    


```python
try:
    num1 = int(input("please enter the num1:"))
    num2 = int(input("please enter the num2:"))
    result = num1/num2
except  ValueError:
    print ("Please enter a number.")
except  ZeroDivisionError:                         # we want to only capture - ValueError and ZeroDivisionError.
    print ("Make sure your denominator is non-zero")
else:
    print ("result is - {}".format(result))
```

    please enter the num1:10
    please enter the num2:0
    Make sure your denominator is non-zero
    
#ex: flipkart big billion day.The flipkart website went down.(transactions,confirmation)
## database lockdown(database guys),server admin(cloud/SA),application(developer)
#ex: A snapdeal -> china market
## snapdeal opened into china -> website was down.(LATIN - english)
# snapdeal -> developers( hyd/bang) -> qwerty(latin) -> madrak/hu
#          -> QA (chennai) -> qwerty


## handling exceptions
# try -> code which can create errors.
# else -> if try block is true(no errors),then what. 
# except -> what to do with exception.
# finally -> 
# case I -> valid values -> try..else..finally
# case II -> invalid value handled by exception -> try..except..finally
# case III -> invalid value not handled by exception -> try..finally..bombed out the program with exception.



```python
# to simuluate an exception
try:
    num1 = int(input("please enter the num1:"))
    num2 = int(input("please enter the num2:"))
    result = num1/num2
except  ValueError:
    print ("Please enter a number.")
else:
    print ("result is - {}".format(result))
finally:
    print ("All is well!!")
    # close the resource
    # file.close()
    # db.close()
    # socket.close()
    
```

    please enter the num1:10
    please enter the num2:0
    All is well!!
    


    ---------------------------------------------------------------------------

    ZeroDivisionError                         Traceback (most recent call last)

    <ipython-input-13-ce7b12994c1c> in <module>
          3     num1 = int(input("please enter the num1:"))
          4     num2 = int(input("please enter the num2:"))
    ----> 5     result = num1/num2
          6 except  ValueError:
          7     print ("Please enter a number.")
    

    ZeroDivisionError: division by zero



```python
# raise

raise SyntaxError("Please clean your glasses")
```


    Traceback (most recent call last):
    

      File "C:\Users\tuxfux\Anaconda3\lib\site-packages\IPython\core\interactiveshell.py", line 3325, in run_code
        exec(code_obj, self.user_global_ns, self.user_ns)
    

      File "<ipython-input-17-31d3298f4b9a>", line 3, in <module>
        raise SyntaxError("Please clean your glasses")
    

      File "<string>", line unknown
    SyntaxError: Please clean your glasses
    



```python
## userdefined exception ( classes )
```
