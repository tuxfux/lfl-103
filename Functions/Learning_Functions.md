
Functions - modular programming
* A block of code,which can be called multiple times.


```python
## create a function
## has no functional parameters/arguments

def my_func():
    print ("hello world")
```


```python
## understanding a function

print (my_func,type(my_func))
```

    <function my_func at 0x000002142D394620> <class 'function'>
    


```python
my_func
```




    <function __main__.my_func()>




```python
# how to call a function
my_func()
```

    hello world
    


```python
# when ever we call a function,function returns a value - value/None
print (my_func())
```

    hello world
    None
    


```python
# return
# return passes the values from function to the main program.
# return is not a print statement
# return marks the end of the function.
# the control goes from the function to the main part of the program.
def my_func():
    return ("hello world")
    print ("Hey there!!")
    print ("How is it today!!")
    print ("what are you upto !!")
```


```python
# call the function
# we returned the "hello world" while calling the programme.
## Main
print (my_func())
```

    hello world
    


```python
### variable namespaces/scope
### global variables and local variables
## locals() basically shows us the namespace/scope.
```


```python
## a is a local variable, defined within a function.
## local variables are available only within function.
## lifetime of my local variable is during runtime of function.
## syntatically there is no way to access the local variable from the main program.
def my_values():
    a = 1
    print (locals())
    return a
```


```python
# call the function
print (my_values())
```

    {'a': 1}
    1
    


```python
## a is not available outside the variable scope.
print (a)
```


    ---------------------------------------------------------------------------

    NameError                                 Traceback (most recent call last)

    <ipython-input-15-cb9bacd097d9> in <module>
    ----> 1 print (a)
    

    NameError: name 'a' is not defined



```python
## global variables
## variable available to us inside the function and also outside
## intially the function will look for x within your function.
## later it will search in global name space
## globals() basically given you a bucket of global variables.
x = 1             # global variable
def my_values():
    print (locals())
    return x
```


```python
# main
print (my_values())
```

    {}
    1
    


```python
print (x)
```

    1
    
In [1]: x = 1

In [2]: globals()
Out[2]:
{'__name__': '__main__',
 '__doc__': 'Automatically created module for IPython interactive environment',
 '__package__': None,
 '__loader__': None,
 '__spec__': None,
 '__builtin__': <module 'builtins' (built-in)>,
 '__builtins__': <module 'builtins' (built-in)>,
 '_ih': ['', 'x = 1', 'globals()'],
 '_oh': {},
 '_dh': ['C:\\Users\\tuxfux'],
 'In': ['', 'x = 1', 'globals()'],
 'Out': {},
 'get_ipython': <bound method InteractiveShell.get_ipython of <IPython.terminal.interactiveshell.TerminalInteractiveShell object at 0x0000027C49037470>>,
 'exit': <IPython.core.autocall.ExitAutocall at 0x27c4a091f98>,
 'quit': <IPython.core.autocall.ExitAutocall at 0x27c4a091f98>,
 '_': '',
 '__': '',
 '___': '',
 '_i': 'x = 1',
 '_ii': '',
 '_iii': '',
 '_i1': 'x = 1',
 'x': 1,
 '_i2': 'globals()'}

In [3]:                                                                                                                 

```python
## banking
```


```python
# case I
amount = 100

def deposit():
    amount = amount + 1000
    return amount
```


```python
print (deposit())
```


    ---------------------------------------------------------------------------

    UnboundLocalError                         Traceback (most recent call last)

    <ipython-input-24-c78eb9baafbc> in <module>
    ----> 1 print (deposit())
    

    <ipython-input-23-8bbc9ed7034b> in deposit()
          3 
          4 def deposit():
    ----> 5     amount = amount + 1000
          6     return amount
    

    UnboundLocalError: local variable 'amount' referenced before assignment



```python
# case II
#amount = 100

def deposit():
    amount = 100
    amount = amount + 1000
    return amount

## case III
def withdraw():
    amount = 100
    amount = amount - 200
    return amount
```


```python
print (deposit())
```

    1100
    


```python
print (withdraw())
```


    ---------------------------------------------------------------------------

    UnboundLocalError                         Traceback (most recent call last)

    <ipython-input-29-94d19e6ca588> in <module>
    ----> 1 print (withdraw())
    

    <ipython-input-27-6191d5eaeaff> in withdraw()
          9 ## case III
         10 def withdraw():
    ---> 11     amount = amount - 200
         12     return amount
    

    UnboundLocalError: local variable 'amount' referenced before assignment



```python
print (withdraw())
```

    -100
    


```python
## global - keyword
### if i want to use a global variable across mutliple functions.
```


```python
amount = 100   # global variable

def deposit():
    global amount         # trying to fetch the global variable for arthimetic operation.
    print (locals())      # show the local namespace
    amount = amount + 1000
    return amount

## case III
def withdraw():
    global amount
    print (locals())
    amount = amount - 200
    return amount
```


```python
print (deposit())
```

    {}
    1100
    


```python
print (amount)
```

    1100
    


```python
print (withdraw())
```

    {}
    900
    


```python
print (amount)
```

    900
    


```python
## functinal parameters
```


```python
def my_add(a,b):
    print (locals())
    return a + b
```


```python
## positional parameters/arguments
```


```python
# Main
print (my_add(11,22))
print (my_add("cloud"," rocks"))
print (my_add("rocks"," cloud"))
```

    {'a': 11, 'b': 22}
    33
    {'a': 'cloud', 'b': ' rocks'}
    cloud rocks
    {'a': 'rocks', 'b': ' cloud'}
    rocks cloud
    


```python
## keybased arguments
```


```python
# Main
print (my_add(a="cloud",b=" rocks"))
print (my_add(b=" rocks",a="cloud"))
```

    {'a': 'cloud', 'b': ' rocks'}
    cloud rocks
    {'a': 'cloud', 'b': ' rocks'}
    cloud rocks
    


```python
## *,**,*args,**kwargs
```
In python are arguments passed as values or references ? 
## arguments are passed as objects. (google)

```python
## *
```


```python
def my_add(a,b):
    return a + b
```


```python
my_list = [11,22]
my_list1 = [11,22,33]
```


```python
# case I
# a = my_list[0]
# b = my_list[1]
# my_add(a,b)

# Case II
print (my_add(my_list))
```


    ---------------------------------------------------------------------------

    TypeError                                 Traceback (most recent call last)

    <ipython-input-3-288eabbe4553> in <module>
          5 
          6 # Case II
    ----> 7 print (my_add(my_list))
    

    TypeError: my_add() missing 1 required positional argument: 'b'



```python
print (my_add(*my_list))
```

    33
    


```python
print (my_add(*my_list1))
```


    ---------------------------------------------------------------------------

    TypeError                                 Traceback (most recent call last)

    <ipython-input-6-ad8af7ea8e2b> in <module>
    ----> 1 print (my_add(*my_list1))
    

    TypeError: my_add() takes 2 positional arguments but 3 were given



```python
## **
```


```python
# a,b are two argument
def my_add(a,b):
    return a + b
```


```python
my_values = {'a':11,'b':12}
my_values1 = {'a':22,'c':33}
```


```python
## how to pass the dictionary to a function.
## a = my_values['a']
#  b = my_values['b']
# my_add(a,b)

# how to unpack dictionary to a function
print (my_add(**my_values)) ## ** will take the values from a dictionary
```

    23
    


```python
print (my_add(**my_values1))
```


    ---------------------------------------------------------------------------

    TypeError                                 Traceback (most recent call last)

    <ipython-input-10-57a0685e03e2> in <module>
    ----> 1 print (my_add(**my_values1))
    

    TypeError: my_add() got an unexpected keyword argument 'c'



```python
## i want to pass keys as arguements to my function
print (my_add(*my_values)) ## * will take the keys from a dictionary.
```

    ab
    


```python
## *args - for arguments
```


```python
## Inbuild function
help(max)
```

    Help on built-in function max in module builtins:
    
    max(...)
        max(iterable, *[, default=obj, key=func]) -> value
        max(arg1, arg2, *args, *[, key=func]) -> value
        
        With a single iterable argument, return its biggest item. The
        default keyword-only argument specifies an object to return if
        the provided iterable is empty.
        With two or more arguments, return the largest argument.
    
    


```python
print (max(11,22,33,44,55,66,101))
print (max(-1,-2,-3,-4))
print (max(32,43,54))
```

    101
    -1
    54
    


```python
## args is for the defination time
## gmax is a user defined function.
def gmax(*args):
    print (args) # args give us values in form of tuples.
```


```python
print (gmax(11,22,33,44,55,66,101))
print (gmax(-1,-2,-3,-4))
print (gmax(32,43,54))
```

    (11, 22, 33, 44, 55, 66, 101)
    None
    (-1, -2, -3, -4)
    None
    (32, 43, 54)
    None
    


```python
def gmax(*args):
    big = -1
    for value in args:
        if value > big:
            big = value
    return big
```


```python
print (gmax(11,22,33,44,55,66,101))
print (gmax(-1,-2,-3,-4))
print (gmax(32,43,54))
```

    101
    -1
    54
    


```python
## **kwargs - key word arguments
```


```python
## call center application
```


```python
def callme(**kwargs):
    return kwargs    # takes keyword arguments and returns as kwargs dictionary.
```


```python
print (callme(name="nag",addr="nijam"))
print (callme(name="nag",email="nag.nijam@gmail.com",maiden="laxmi"))
print (callme(email="nag.nijam@gmail.com",maiden="laxmi",gender="m"))
```

    {'name': 'nag', 'addr': 'nijam'}
    {'name': 'nag', 'email': 'nag.nijam@gmail.com', 'maiden': 'laxmi'}
    {'email': 'nag.nijam@gmail.com', 'maiden': 'laxmi', 'gender': 'm'}
    


```python
def callme(**kwargs):
    for keys in kwargs.keys():
        print ("key:{}".format(kwargs[keys]))
    return ""
```


```python
print (callme(name="nag",addr="nijam"))
print (callme(name="nag",email="nag.nijam@gmail.com",maiden="laxmi"))
print (callme(email="nag.nijam@gmail.com",maiden="laxmi",gender="m"))
```

    key:nag
    key:nijam
    
    key:nag
    key:nag.nijam@gmail.com
    key:laxmi
    
    key:nag.nijam@gmail.com
    key:laxmi
    key:m
    
    


```python
# default
# ex: max_value is a default argument
```


```python
def my_multi(num,max_value=10):
    for value in range(1,max_value + 1):
        print ("{} * {} = {}".format(num,value,num*value))
```


```python
my_multi(2)
```

    2 * 1 = 2
    2 * 2 = 4
    2 * 3 = 6
    2 * 4 = 8
    2 * 5 = 10
    2 * 6 = 12
    2 * 7 = 14
    2 * 8 = 16
    2 * 9 = 18
    2 * 10 = 20
    


```python
my_multi(10,5)
```

    10 * 1 = 10
    10 * 2 = 20
    10 * 3 = 30
    10 * 4 = 40
    10 * 5 = 50
    


```python
## putty
def putty(hostname,port=22):
    pass
## putty(host1)
## putty(host2,23) or putty(hostname=host2,port=23)
```


```python
### Function within a function.
```


```python
def upper():      # upper function
        x = 1     # x is local variable to upper,x is global to inner function.
        def inner():
            return x
        return inner() # calling the inner() function.returning the output of the inner function.
```


```python
print (upper()) # 1
```


```python
## inner() is defined within the runtime of upper() function.
## variable defined inside a function exists during the runtime of the function.
## inner() was not existing
print (inner()) # Error
```


    ---------------------------------------------------------------------------

    NameError                                 Traceback (most recent call last)

    <ipython-input-42-7fdfdce56c3e> in <module>
          2 ## variable defined inside a function exists during the runtime of the function.
          3 ## inner() was not existing
    ----> 4 print (inner()) # Error
    

    NameError: name 'inner' is not defined



```python
print (x)
```


    ---------------------------------------------------------------------------

    NameError                                 Traceback (most recent call last)

    <ipython-input-43-606ad02f996c> in <module>
    ----> 1 print (x)
    

    NameError: name 'x' is not defined



```python
## closures - decorators
```


```python
def upper():      # upper function
        x = 1     # x is local variable to upper,x is global to inner function.
        def inner():
            return x
        return inner  ## returning the  address of inner function
```


```python
my_value = upper() # the upper() function is being called and the return value is going to be placed in my_value.
```


```python
'''
Address of inner function.
        def inner():
            return x
'''
print (my_value,type(my_value))
```

    <function upper.<locals>.inner at 0x000002782BF22BF8> <class 'function'>
    


```python
## againts our expectation the function succeded.
## closure - during the defination of any function, the function address holds the namespace(local/global) of the global and
## local variables
print (my_value()) # inner() # error,address
```

    1
    


```python
## function are first class objects
## string,numbers,floats can be passed to a function.
```


```python
# my_add
def my_add(a,b):
    return (a + b)

# my_sub
def my_sub(a,b):
    if a > b:
        return (a - b)
    else:
        return (b - a)

# my_extra
def my_extra(func,a,b):
    return func(a,b)
```


```python
print (my_add(11,22))
```

    33
    


```python
print (my_extra(my_add,11,22))
```

    33
    


```python
##  map,filter and lamda
```


```python
# 3.x
help(map)
```

    Help on class map in module builtins:
    
    class map(object)
     |  map(func, *iterables) --> map object
     |  
     |  Make an iterator that computes the function using arguments from
     |  each of the iterables.  Stops when the shortest iterable is exhausted.
     |  
     |  Methods defined here:
     |  
     |  __getattribute__(self, name, /)
     |      Return getattr(self, name).
     |  
     |  __iter__(self, /)
     |      Implement iter(self).
     |  
     |  __next__(self, /)
     |      Implement next(self).
     |  
     |  __reduce__(...)
     |      Return state information for pickling.
     |  
     |  ----------------------------------------------------------------------
     |  Static methods defined here:
     |  
     |  __new__(*args, **kwargs) from builtins.type
     |      Create and return a new object.  See help(type) for accurate signature.
    
    
# 2.x
In [1]: map?
Docstring:
map(function, sequence[, sequence, ...]) -> list

Return a list of the results of applying the function to the items of
the argument sequence(s).  If more than one sequence is given, the
function is called with an argument list consisting of the corresponding
item of each sequence, substituting None for missing values when not all
sequences have the same length.  If the function is None, return a list of
the items of the sequence (or a list of tuples if more than one sequence).
Type:      builtin_function_or_method

In [2]:

```python
# square function
def square(a):
    return a * a
```


```python
print (square(2))
print (square(4))
```

    4
    16
    


```python
# 3.x
map(square,[11,22,33,44,55,66])  # intermediate values
```




    <map at 0x1a24c7610f0>




```python
for value in map(square,[11,22,33,44,55,66]):
    print (value)
```

    121
    484
    1089
    1936
    3025
    4356
    
# 2.x

In [6]: def square(a):
   ...:     return a * a
   ...:
   ...:

In [7]: map(square,[11,22,33,44,55,66])
Out[7]: [121, 484, 1089, 1936, 3025, 4356]


```python
# filter

# 3.x
help(filter)
```

    Help on class filter in module builtins:
    
    class filter(object)
     |  filter(function or None, iterable) --> filter object
     |  
     |  Return an iterator yielding those items of iterable for which function(item)
     |  is true. If function is None, return the items that are true.
     |  
     |  Methods defined here:
     |  
     |  __getattribute__(self, name, /)
     |      Return getattr(self, name).
     |  
     |  __iter__(self, /)
     |      Implement iter(self).
     |  
     |  __next__(self, /)
     |      Implement next(self).
     |  
     |  __reduce__(...)
     |      Return state information for pickling.
     |  
     |  ----------------------------------------------------------------------
     |  Static methods defined here:
     |  
     |  __new__(*args, **kwargs) from builtins.type
     |      Create and return a new object.  See help(type) for accurate signature.
    
    


```python
# 3.x

def my_even(a):
    if a % 2 == 0:
        return 'even'
    
# filter
print (filter(my_even,range(1,21))) #filter object

for value in filter(my_even,range(1,21)):
    print (value)
```

    <filter object at 0x000001A24C761940>
    2
    4
    6
    8
    10
    12
    14
    16
    18
    20
    
# 2.x

In [8]: help(filter)
Help on built-in function filter in module __builtin__:

filter(...)
    filter(function or None, sequence) -> list, tuple, or string

    Return those items of sequence for which function(item) is true.  If
    function is None, return the items that are true.  If sequence is a tuple
    or string, return the same type, else return a list.In [12]: # function is true given it returns a values

In [13]: def my_even(a):
    ...:     if a % 2 == 0:
    ...:         return 'even'
    ...:

In [14]: print my_even(4)
even

In [15]: print my_even(3)
None

In [16]: print filter(my_even,range(1,21))
[2, 4, 6, 8, 10, 12, 14, 16, 18, 20]


```python
# lambda - creating of nameless functions
# lambda works in conjunction with map and filter.
```


```python
# square - 3.x
print (map(square,[11,22,33,44,55,66]))

for value in map(square,[11,22,33,44,55,66]):
    print (value)
```

    <map object at 0x000001A24C761C88>
    121
    484
    1089
    1936
    3025
    4356
    
# square function
def square(a):
    return a * a

```python
print (map(lambda a:a*a,[11,22,33,44,55,66]))
```

    <map object at 0x000001A24C761198>
    


```python
for value in map(lambda a:a*a,[11,22,33,44,55,66]):
    print (value)
```

    121
    484
    1089
    1936
    3025
    4356
    


```python
# filter

# filter
print (filter(my_even,range(1,21))) #filter object

for value in filter(my_even,range(1,21)):
    print (value)
```

    <filter object at 0x000001A24C772208>
    2
    4
    6
    8
    10
    12
    14
    16
    18
    20
    
def my_even(a):
    if a % 2 == 0:
        return 'even'

```python
print (filter(lambda a:a%2 == 0,range(1,21)))
```

    <filter object at 0x000001A24C71DC50>
    


```python
for value in (filter(lambda a:a%2 == 0,range(1,21))):
    print (value)
```

    2
    4
    6
    8
    10
    12
    14
    16
    18
    20
    


```python
## exercise
```
# square function
def square(a):
    return a * adef my_even(a):
    if a % 2 == 0:
        return 'even'In [19]: map(square,range(1,6))
Out[19]: [1, 4, 9, 16, 25]

In [20]: filter(square,range(1,6))
Out[20]: [1, 2, 3, 4, 5]

In [21]: filter(my_even,range(1,6))
Out[21]: [2, 4]

In [22]: map(my_even,range(1,6))
Out[22]: [None, 'even', None, 'even', None]

In [23]: filter(my_even,range(-1,1))
Out[23]: [0]
