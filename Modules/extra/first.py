#!/usr/bin/env python
# -*- coding: utf-8 -*-

version = 2.0

## functions
def my_add(a,b): # __main__.my_add
    ''' This is for addition of two numbers and string '''
    return a + b

def my_sub(a,b):
    ''' This is for substraction of two numbers '''
    if a > b:
        return a - b
    else:
        return b - a
    
def my_div(a,b):
    ''' This is for division of two numbers '''
    return a/b

def  my_multi(a,b):
    ''' This is for multiplication of numbers '''
    return a * b
    

## Main
if __name__ == '__main__':         # the part below should never be imported.
    print ("LAUNCHING A MISSILE")