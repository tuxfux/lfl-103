#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
sys.path.insert(0,'C:\\Users\\tuxfux\\Documents\\lfl-python103\\Modules\\extra')
import first as f

def my_add(a,b):   
    ''' to add two numbers '''
    a = int(a)
    b = int(b)
    return a + b

# Main
if __name__ == '__main__':
    print ("Addition of two numbers:{} ".format(my_add(11,22)))
    print ("Addition of two string:{} ".format(f.my_add("python"," rocks")))
    
    
# my_add -> __main__.my_add
# f.my_add -> first.my_add

