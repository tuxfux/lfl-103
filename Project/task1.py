#!/usr/bin/python
# -*- coding: utf-8 -*-
#http://www.pythonchallenge.com/pc/def/map.html
import string

my_alpha = list(string.ascii_lowercase)
new_string = []
#my_string = """
#g fmnc wms bgblr rpylqjyrc gr zw fylb. rfyrq ufyr amknsrcpq ypc dmp. bmgle gr gl zw fylb gq glcddgagclr ylb rfyr'q ufw rfgq rcvr gq qm jmle. sqgle qrpgle.kyicrpylq() gq pcamkkclbcb. lmu ynnjw ml rfc spj.
#"""
my_string="map"

## lets read the string and compute.
for letter in my_string:
    # my_alpha.index('g') + 2
    if letter in my_alpha:
        if letter == 'z':
            new_alpha = 'b'
        elif letter == 'y':
            new_alpha = 'a'
        else:
            new_alpha = my_alpha[my_alpha.index(letter) + 2]
        new_string.append(new_alpha)
    else:
        new_string.append(letter)
        
print ("".join(new_string))




