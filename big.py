#!/usr/bin/python
# -*- coding: utf-8 -*-
# and operation -> both conditions should be true.
# or operator -> one of the conditions should be true.

num1 = int(input("please enter the num1:"))
num2 = int(input("please enter the num2:"))
num3 = int(input("please enter the num3:"))

if num1 > num2 and num1 > num3:
    print ("{0} is greater than {1} and {2}".format(num1,num2,num3))
elif num2 > num1 and num2 > num3:
    print ("{1} is greater than {0} and {2}".format(num1,num2,num3))
elif num3 > num1 and num3 > num2:
    print ("{2} is greater than {1} and {0}".format(num1,num2,num3))
else:
        print ("{0} , {1} and {2} are equals".format(num1,num2,num3))

## docstring
## task
"""
(base) C:\Users\tuxfux\Documents\lfl-python103>python big.py
please enter the num1:11
please enter the num2:11
please enter the num3:10
11 , 11 and 10 are equals
"""
