why Python ? 1990
python -> 2.x and 3.x
* python is opensource language.
* python is a full fledged oop/functional based language.
* python is platform independent

## Linux (ubuntu/zorion/linuxmint/debain/suse)
# sudo apt-get install python (2.x)
# sudo apt-get install python3 (3.x)
# sudo apt-get install ipython (2.x)
# sudo apt-get install ipython3 (3.x)
# sudo apt-get install bpython  (2.x)
# sudo apt-get install bpython3 (3.x)

## Windows
https://docs.anaconda.com/anaconda/install/windows/
https://www.anaconda.com/distribution/#windows

## IDE
https://stackoverflow.com/questions/81584/what-ide-to-use-for-python
ex: sypder,sublime text,pycharm,komodo,atom

write code -> editor -> underlying programing language

references:
https://ipython.org/
https://bpython-interpreter.org/
