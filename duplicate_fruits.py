#!/usr/bin/python
# -*- coding: utf-8 -*-

my_fruits = ["apple","banana","banana","banana","cherry","cherry","cherry","guava"]
#my_fruits = ["apple","banana","banana","cherry","cherry","cherry","guava"]
# my_dupli = ["banana","cherry"]
# my_fruits = ["apple","banana","cherry","guava"]

my_dupli=[]
for fruit in my_fruits[:]:
    print (" Fruit from the list - {}".format(fruit))
    if my_fruits.count(fruit) > 1:
        if fruit not in my_dupli:
            print ("Adding the fruit - {}".format(fruit))
            my_dupli.append(fruit)
        print ("removing the fruit - {}".format(fruit))
        my_fruits.remove(fruit)
        
print (my_dupli)
print (my_fruits)

"""
my_fruits = ["apple","banana","banana","banana","cherry","cherry","cherry","guava"]
my_fruits[:] = ["apple","banana","banana","banana","cherry","cherry","cherry","guava"]
"""
        
