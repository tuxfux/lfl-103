#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: tuxfux
# Usage: I want to say hello to the world.
# creation: 02-12-2019
# log: 03-12-2019 - tuxfux - i made so and so modification


## notes
# why do we save our code in .py formate.
# 1. convience.
# 2. platform independent - windows and linux.
# 3. modular programming - you cannot use as a module if you dont have .py extension.
# #!/usr/bin/python - shabang
# https://docs.python.org/3.7/tutorial/interpreter.html#source-code-encoding

print ("hello world !!! ")

#tuxfux@DESKTOP-8B46PBJ:/mnt/c/Users/tuxfux/Documents/lfl-python103$ python first.py
#hello world !!!
#tuxfux@DESKTOP-8B46PBJ:/mnt/c/Users/tuxfux/Documents/lfl-python103$ perl first.py
#hello world !!! tuxfux@DESKTOP-8B46PBJ:/mnt/c/Users/tuxfux/Documents/lfl-python103$
#tuxfux@DESKTOP-8B46PBJ:/mnt/c/Users/tuxfux/Documents/lfl-python103$
#tuxfux@DESKTOP-8B46PBJ:/mnt/c/Users/tuxfux/Documents/lfl-python103$ uname -a
#Linux DESKTOP-8B46PBJ 4.4.0-18362-Microsoft #476-Microsoft Fri Nov 01 16:53:00 PST 2019 x86_64 x86_64 x86_64 GNU/Linux
#tuxfux@DESKTOP-8B46PBJ:/mnt/c/Users/tuxfux/Documents/lfl-python103$
#tuxfux@DESKTOP-8B46PBJ:/mnt/c/Users/tuxfux/Documents/lfl-python103$ ls -l
#total 12
#-rwxrwxrwx 1 tuxfux tuxfux 7046 Dec  2 18:59 Day02-Getting_started_help.txt
#-rwxrwxrwx 1 tuxfux tuxfux  818 Nov 28 19:27 day01-Installation.txt
#-rwxrwxrwx 1 tuxfux tuxfux  143 Nov 28 21:35 email.txt
#-rwxrwxrwx 1 tuxfux tuxfux  443 Dec  2 19:11 first.py
#tuxfux@DESKTOP-8B46PBJ:/mnt/c/Users/tuxfux/Documents/lfl-python103$ ./first.py
#./first.py: line 6: $'\r': command not found
#./first.py: line 7: $'\r': command not found
#./first.py: line 14: $'\r': command not found
#./first.py: line 15: syntax error near unexpected token `"hello world !!! "'
#'/first.py: line 15: `print ("hello world !!! ")
#tuxfux@DESKTOP-8B46PBJ:/mnt/c/Users/tuxfux/Documents/lfl-python103$ which python
#/usr/bin/python
#tuxfux@DESKTOP-8B46PBJ:/mnt/c/Users/tuxfux/Documents/lfl-python103$ ./first.py
#hello world !!!
#tuxfux@DESKTOP-8B46PBJ:/mnt/c/Users/tuxfux/Documents/lfl-python103$ python --version
#Python 2.7.15+
#tuxfux@DESKTOP-8B46PBJ:/mnt/c/Users/tuxfux/Documents/lfl-python103$ python3 --version
#Python 3.6.9
#tuxfux@DESKTOP-8B46PBJ:/mnt/c/Users/tuxfux/Documents/lfl-python103$ which python3
#/usr/bin/python3
#tuxfux@DESKTOP-8B46PBJ:/mnt/c/Users/tuxfux/Documents/lfl-python103$ ./first.py
#  File "./first.py", line 16
#    print "hello world !!! "
#                           ^
#SyntaxError: Missing parentheses in call to 'print'. Did you mean print("hello world !!! ")?
#tuxfux@DESKTOP-8B46PBJ:/mnt/c/Users/tuxfux/Documents/lfl-python103$ ./first.py
#hello world !!!
#tuxfux@DESKTOP-8B46PBJ:/mnt/c/Users/tuxfux/Documents/lfl-python103$
