#!/usr/bin/python
# -*- coding: utf-8 -*-
# break - to take out of the loop.It will not take me out of the program.
# sys.exit - exit will take me out of the program.
# sys.exit?
# return

import sys

#test = True
my_num = 7 # number on my palm
## task1: randomize the my_num number.
## task2: if someone is not guessing in 3 times.
##      : exit out - thanks buddy .. better luck next time.

for_game = input("Do you want to play the game ?")
if for_game == 'no':
    sys.exit()


#while test:  # condition/ condition has to be true
while True:
    guess = int(input("please guess the number:"))
    
    if guess == my_num:
        print ("Congo!! you guessed the right number")
        #test = False
        break
        #sys.exit()
    elif guess > my_num:
        print ("Buddy you guessed a slightly larger number")
    elif guess < my_num:
        print ("Buddy you guessed a slightly smaller number")
 
print ("thank for playing the game")



"""
upper loop
   lower loop
      if <condition satisfied>
       #break
       exit
"""





