#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
my_days = ["yesterday","today","tomorrow","dayafter"]

task1:
output -> 
Yesterday
TOday
TOMorrow
DAYAfter

"""

my_days = ["yesterday","today","tomorrow","dayafter"]

for value in my_days:
    #print ("{} -> {} -> {}".format(value,my_days.index(value),value[my_days.index(value)]))
    #print ("{} -> {} -> {}".format(value,my_days.index(value),value[0:my_days.index(value) + 1]))
    print (value[:my_days.index(value) + 1].upper() + value[my_days.index(value) + 1:])
